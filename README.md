# Knowledge Based Sequential Decision Support

This project provides the source code related to the paper "[Comparison and Incorporation of Reasoning and Learning Approaches for Cancer Therapy Research](https://doi.org/10.3233/SHTI230709)" by Andre Thevapalan, Daan Apeldoorn et al.
The project is licensed under the GNU General Public License (GPL) version 3.

NOTE: THIS SCIENTIFIC SOFTWARE SERVES AS A PROOF-OF-CONCEPT WHICH IS NOT INTENDED TO BE USED OUT OF THE BOX IN A PRODUCTIVE SETTING! 
IT COMES WITH DUMMY DATA THAT NEEDS TO BE ADAPTED; FURTHER ADAPTIONS MIGHT BE NECESSARY.

## Introduction

The project represents a framework for the creation of a knowledge based sequential decision support system which is able to provide explanations for the inferred decision sequences. The framework is based on/involves the InteKRator toolbox. 
While the approach might in principle be used in different domains, it is elaborated here (according to the aforementioned paper) for the recommendation of cancer therapy plans inferred from provided patient attribute values.

The software provides:
- A framework for the creation of a knowledge based sequential decision support system, i.e., a system that allows for inferring multiple alternative sequences (such as therapy plans) from provided input attribute values (e.g., features of a patient) and which is able to explain the provided decisions.
- A checking option, which allows for checking the inferred alternatives against a data set of evident results.

## Getting started

Before getting started, the user should read the paper "Comparison and Incorporation of Reasoning and Learning Approaches for Cancer Therapy Research" by Andre Thevapalan, Daan Apeldoorn et al. to get the basic ideas, abbreviations, etc. (Since the paper is not yet published, this page will be updated and will link to the paper as soon as it is available.)

### Framework

The framework comprises several knowledge bases in the form of text files each comprising the knowledge about one step in the decision sequence (here: one therapy in a therapy plan, i.e., "CT.kb" --> "AbT_PRE.kb" --> "OP.kb" --> "AbT.kb" --> "AHT.kb").
The decision in every step is inferred from the provided input attribute values _and_ the decision(s) of the previous step(s). Since multiple alternative decisions can be inferred in one step, this may result in an inference tree with the complete decision sequences (i.e., therapy plans) being represented by the paths form the tree's root to its leaves.
Note that the decisions inferred in a step comprise a numeric prefix indicating the position in the descition sequence. 

Each knowledge bases can be learned from a corresponding data set using the InteKRator toolbox. Such a data set consists of one column for each input attribute (e.g., representing a patient feature) _and_ additional columns for the results of each previous step. 

The input attribute values can be provided as strings in the "main" method's "args" array. 
The output is provided on the console.

### Checking

It is possible to check the inferred sequences against a data set of (potentially) evident examples (in the context of the aforementioned paper, this is used to compare inferred therapy plans against those that could be inferred from an answer set program).

For this purpose, provide the evident examples in the file "answer_sets.txt" and comment in the corresponding call of the "check" method in the main file ("SequentialDecisionSupport.java").
The file "name_mapping.txt" can be used to define a simple mapping from names used in the evident examples to the ones used by the system. 

If the inferred decision sequences do not exactly fit to the corresponding decision sequences provided in the file (e.g., not the same alternative therapy plans are inferred for a patient), an additional output indicating that will be provided on the console.
