/*
Framework for sequential decision support with explanations
NOTE: THIS SCIENTIFIC SOFTWARE SERVES AS A PROOF-OF-CONCEPT WHICH 
IS NOT INTENDED TO BE USED OUT OF THE BOX IN A PRODUCTIVE SETTING! 
IT COMES WITH DUMMY DATA THAT NEEDS TO BE ADAPTED; FURTHER ADAPTIONS
MIGHT BE NECESSARY.
Copyright (C) 2023  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)
                    (further contributers helped indirectly to
                    create this software)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


package sequentialdecisionsupport;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import intekrator.representation.MultiAbstractionLevelKB;
import intekrator.representation.Rule;


public class SequentialDecisionSupport 
{
	public static void main( String[] args ) 
	{
		// COMMENT THIS IN TO CHECK WHETHER THE INFERENCES CONFORM TO THE PROVIDED ONES
		check();
		
		// Collect patient information
		Set<String> patientInfo = new HashSet<String>();
		for( String arg : args )
		{
			patientInfo.add( arg );
		}

		// The set of possible therapies, each consisting of one or more ordered treatments
		Map<List<String>, List<Set<Rule>>> explanations = new HashMap<List<String>, List<Set<Rule>>>();
		Set<List<String>> therapies = inferTherapies( patientInfo, explanations ); 
		
		// Output therapies
		for( List<String> therapy : therapies )
		{
			List<String> therapyAsAnswerSet = therapyAsAnswerList( therapy );
			for( String treatment : therapyAsAnswerSet )
			{
				System.out.print( treatment + "     " );
			}
			System.out.println();
			
			int treatmentNo = 0;
			for( String treatment : therapy )
			{
				String explanation = "" + explanations.get( therapy ).get( treatmentNo );
				explanation = explanation.substring( 1, explanation.length() - 1 );

				// Transform treatment name
				String[] treatmentNameParts = treatment.split( "_" );
				String treatmentPosition = treatmentNameParts[ 1 ].substring( 0, 2 );
				String transformedTreatmentName = transformTreatmentName( treatmentNameParts, treatmentPosition );
				
				System.out.println( transformedTreatmentName + ": " + explanation );
				
				treatmentNo++;
			}
			System.out.println();
		}
	}
	
	
	public static void check()
	{
		// Read Mapping into a HashMap
		Map<String, String> nameMapping = new HashMap<String, String>();
		try 
		{
			// Create reader
			BufferedReader reader = new BufferedReader( new FileReader( "name_mapping.txt" ) );
		    			
			// Read the first line
			String line = reader.readLine();
		    
		    // Go through every line
		    while( line != null ) 
		    {		    	
		    	// Skip empty lines
		    	if( !line.equals( "" ) )
		    	{
		    		String[] keyValue = line.split( "=" );
		    		nameMapping.put( keyValue[ 0 ].trim(), keyValue[ 1 ].trim() );
		    	}
		    	
		    	// Read next line
		    	line = reader.readLine();
		    }

		    // Close reader
		    reader.close();
		} 
		catch( FileNotFoundException e ) 
		{
			e.printStackTrace();
		} 
		catch( IOException e ) 
		{
			e.printStackTrace();
		}
		
		// Read original answer sets into a HashMap
		Map<Set<String>, Set<Set<String>>> answerSets = new LinkedHashMap<Set<String>, Set<Set<String>>>();
		try 
		{
			// Create reader
			BufferedReader reader = new BufferedReader( new FileReader( "answer_sets.txt" ) );
		    			
			// Read the first line
			String line = reader.readLine();
		    
		    // Go through every line
		    while( line != null ) 
		    {		    	
		    	// If a new patient starts
		    	if( line.startsWith( "patient" ) )
		    	{
			    	// DEBUG
			    	System.out.println( line );

		    		// Skip line
					line = reader.readLine();

					// Read and process input to create the key
					line = reader.readLine();
					String[] inputs = line.substring( 2, line.length() - 2 ) .split( "," );
					Set<String> inputsSet = new HashSet<String>();
					for( String input : inputs )
					{
						String mappedName = nameMapping.get( input );
						inputsSet.add( mappedName );
					}
					
		    		// Skip line
					line = reader.readLine();

					// Read the first answer set line
					line = reader.readLine();
				    
				    // Go through every line
					Set<Set<String>> outputsSetsSet = new HashSet<Set<String>>();
				    while( !line.isEmpty() ) 
				    {
				    	// DEBUG
				    	System.out.println( line );
				    	
						String[] outputs = line.substring( 2, line.length() - 2 ) .split( "," );
						Set<String> outputsSet = new HashSet<String>();
						for( String output : outputs )
						{
							if( !output.isEmpty() )
							{
								// INFO
								if( !nameMapping.containsKey( output.substring( 2, output.length() ) ) )
								{
									System.out.println( "WARNING: The key " + output.substring( 2, output.length() ) + " is not contained in the name mapping!" );
								}
								
								String mappedName = output.substring( 0, 2 ) + nameMapping.get( output.substring( 2, output.length() ) );
								outputsSet.add( mappedName );
							}
						}
					    outputsSetsSet.add( outputsSet );

					    // Skip line
						line = reader.readLine();
						
					    // Read next answer set line
						line = reader.readLine();
				    }

				    // Create input key with corresponding output as value
				    answerSets.put( inputsSet, outputsSetsSet );
		    	}
		    	
		    	// Read next line
		    	line = reader.readLine();
		    }

		    // Close reader
		    reader.close();
		} 
		catch( FileNotFoundException e ) 
		{
			e.printStackTrace();
		} 
		catch( IOException e ) 
		{
			e.printStackTrace();
		}
		
		// INFERENCE TIME MEASUREMENT
		long timeNeededForInferenceSum = 0;

		// Run through every patient
		int patientNo = 0;
		for( Set<String> input : answerSets.keySet() )
		{
			patientNo++;
			System.out.println( patientNo );
			
			// INFERENCE TIME MEASUREMENT
			long timeBeforeInference = System.currentTimeMillis();
			
			// Extent the set with unknown values
			Set<String> inputSetWithUnknownValues = extendWithUnknownValues( input );
			
			// Create inferences
			Map<List<String>, List<Set<Rule>>> explanations = new HashMap<List<String>, List<Set<Rule>>>();
			Set<List<String>> therapies = inferTherapies( inputSetWithUnknownValues, explanations );
			
			// INFERENCE TIME MEASUREMENT
			timeNeededForInferenceSum += (System.currentTimeMillis() - timeBeforeInference);
			
			// Create original answer sets
			Set<Set<String>> answerSetThearpies = answerSets.get( input );
			
			// Check if the provided therapies are equal:
			if( !areTherapiesEqual( therapies, answerSetThearpies ) )
			{
				System.out.println( "THE ANSWER SET OF PATIENT " + patientNo + " COULD NOT BE REPRODUCED!" );
			}
		}
		
		// INFERENCE TIME MEASUREMENT
		System.out.println( "AVG. TIME NEEDED FOR INFERENCES: " + (((double) timeNeededForInferenceSum) / answerSets.size() / 1000) + " sec." );
	}
	
	
	public static Set<String> extendWithUnknownValues( Set<String> input )
	{
		// Create all unknown values
		Set<String> allUnknownValues = new HashSet<String>();
		allUnknownValues.add( "age_UNKNOWN" );
		allUnknownValues.add( "brca_UNKNOWN" );
		allUnknownValues.add( "ci_UNKNOWN" );
		allUnknownValues.add( "epr_UNKNOWN" );
		allUnknownValues.add( "existingCC_UNKNOWN" );
		allUnknownValues.add( "gender_UNKNOWN" );
		allUnknownValues.add( "gmc_UNKNOWN" );
		allUnknownValues.add( "her_UNKNOWN" );
		allUnknownValues.add( "lndCount_UNKNOWN" );
		allUnknownValues.add( "lobular_UNKNOWN" );
		allUnknownValues.add( "menopausal_UNKNOWN" );
		allUnknownValues.add( "NEG_ci_UNKNOWN" );
		allUnknownValues.add( "nodal_UNKNOWN" );
		allUnknownValues.add( "risk_UNKNOWN" );
		allUnknownValues.add( "schedule_UNKNOWN" );
		allUnknownValues.add( "tumorSize_UNKNOWN" );
		allUnknownValues.add( "CT_0.UNKNOWN" );
		allUnknownValues.add( "AbT-PRE_0.UNKNOWN" );
		allUnknownValues.add( "OP_0.UNKNOWN" );
		allUnknownValues.add( "AbT_0.UNKNOWN" );
		
		// Extend set with unknown values
		Set<String> inputSetWithUnknownValues = new HashSet<String>( input );
		for( String unknownValue : allUnknownValues )
		{
			String valuePrefix = unknownValue.split( "_" )[ 0 ];
			boolean isValueContained = false;
			for( String value : inputSetWithUnknownValues )
			{
				if( value.startsWith( valuePrefix ) )
				{
					isValueContained = true;
					break;
				}
			}
			if( !isValueContained )
			{
				inputSetWithUnknownValues.add( unknownValue );
			}
		}
		
		return inputSetWithUnknownValues;
	}
	
	
	public static boolean isTreatmentContained( String treatment, Set<String> answerSet )
	{
		if( answerSet.contains( treatment ) )
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	
	public static boolean areTherapiesEqual( Set<List<String>> therapies, Set<Set<String>> answerSets )
	{
		if( therapies.size() != answerSets.size() )
		{
			return false;
		}
		
		for( List<String> therapy : therapies )
		{
			if( !isTherapyContained( therapy, answerSets ) )
			{
				return false;
			}
		}
		
		return true;
	}
	
	
	public static Set<String> therapyAsAnswerSet( List<String> therapy )
	{
		Set<String> therapyAsSet = new HashSet<String>( therapyAsAnswerList( therapy ) );

		return therapyAsSet;
	}
	
	
	public static String transformTreatmentName( String[] treatmentNameParts, String treatmentPosition )
	{
		return treatmentPosition + treatmentNameParts[ 0 ] + "_" + treatmentNameParts[ 1 ].substring( 2, treatmentNameParts[ 1 ].length() );
	}

	
	public static List<String> therapyAsAnswerList( List<String> therapy )
	{
		List<String> therapyAsAnswerList = new ArrayList<String>();
		for( String treatment : therapy )
		{
			String[] treatmentNameParts = treatment.split( "_" );
			String treatmentPosition = treatmentNameParts[ 1 ].substring( 0, 2 );
			if( !treatmentPosition.equals( "0." ) )
			{
				String transformedTreatmentName = transformTreatmentName( treatmentNameParts, treatmentPosition );
				therapyAsAnswerList.add( transformedTreatmentName );
			}
		}
		
		return therapyAsAnswerList;
	}
	
	
	public static boolean isTherapyContained( List<String> therapy, Set<Set<String>> answerSets )
	{
		Set<String> therapyAsSet = therapyAsAnswerSet( therapy );
		
		if( answerSets.contains( therapyAsSet ) )
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	
	private static Set<String> getInferences( List<String> results )
	{
		Set<String> inferences = new HashSet<String>();
		for( String result : results )
		{
			String[] split = result.split( "_" );
			for( int i = 1; i < split.length; i++ )
			{
				inferences.add( split[ 0 ] + "_" + split[ i ] );
			}
		}
		
		return inferences;
	}
	
	
	private static Set<List<String>> inferTherapies( Set<String> patientInfo, Map<List<String>, List<Set<Rule>>> explanations )
	{
		// Read KBs
		MultiAbstractionLevelKB kbCT = new MultiAbstractionLevelKB( "CT.kb" );
		MultiAbstractionLevelKB kbAbTPRE = new MultiAbstractionLevelKB( "AbT-PRE.kb" );
		MultiAbstractionLevelKB kbOP = new MultiAbstractionLevelKB( "OP.kb" );
		MultiAbstractionLevelKB kbAbT = new MultiAbstractionLevelKB( "AbT.kb" );
		MultiAbstractionLevelKB kbAHT = new MultiAbstractionLevelKB( "AHT.kb" );
		
		// The set of possible therapies, each consisting of one or more ordered treatments
		Set<List<String>> therapies = new HashSet<List<String>>(); 
		
		// Do inference for first KB (CT)
		Set<Rule> explanationsCT = new HashSet<Rule>();
		List<String> resultsCT = kbCT.reasoning( patientInfo, explanationsCT );
		
		// Create a list of inferences (merge multiple equivalent inferences)
		Set<String> inferencesCT = getInferences( resultsCT );
		
		// For each inference from first KB
		for( String inferenceCT : inferencesCT )
		{
			// Create new collected information with added inference
			Set<String> patientInfo_CT = new HashSet<String>( patientInfo );
			patientInfo_CT.add( inferenceCT );

			// Do inference for second KB (AbT-PRE)
			Set<Rule> explanationsAbTPRE = new HashSet<Rule>();
			List<String> resultsAbTPRE = kbAbTPRE.reasoning( patientInfo_CT, explanationsAbTPRE );
		
			// Create a list of inferences (merge multiple equivalent inferences)
			Set<String> inferencesAbTPRE = getInferences( resultsAbTPRE );

			// For each inference from second KB
			for( String inferenceAbTPRE : inferencesAbTPRE )
			{	
				// Create new collected information with added inference
				Set<String> patientInfo_CT_AbTPRE = new HashSet<String>( patientInfo_CT );
				patientInfo_CT_AbTPRE.add( inferenceAbTPRE );

				// Do inference for third KB (OP)
				Set<Rule> explanationsOP = new HashSet<Rule>();
				List<String> resultsOP = kbOP.reasoning( patientInfo_CT_AbTPRE, explanationsOP );
			
				// Create a list of inferences (merge multiple equivalent inferences)
				Set<String> inferencesOP = getInferences( resultsOP );
	
				// For each inference from third KB
				for( String inferenceOP : inferencesOP )
				{
					// Create new collected information with added inference
					Set<String> patientInfo_CT_AbTPRE_OP = new HashSet<String>( patientInfo_CT_AbTPRE );
					patientInfo_CT_AbTPRE_OP.add( inferenceOP );
				
					// Do inference for fourth KB (AbT)
					Set<Rule> explanationsAbT = new HashSet<Rule>();
					List<String> resultsAbT = kbAbT.reasoning( patientInfo_CT_AbTPRE_OP, explanationsAbT );
					
					// Create a list of inferences (merge multiple equivalent inferences)
					Set<String> inferencesAbT = getInferences( resultsAbT );
					
					// For each inference from fourth KB
					for( String inferenceAbT : inferencesAbT )
					{
						// Create new collected information with added inference
						Set<String> patientInfo_CT_AbTPRE_OP_AbT = new HashSet<String>( patientInfo_CT_AbTPRE_OP );
						patientInfo_CT_AbTPRE_OP_AbT.add( inferenceAbT );
					
						// Do inference for fifth KB (AHT)
						Set<Rule> explanationsAHT = new HashSet<Rule>();
						List<String> resultsAHT = kbAHT.reasoning( patientInfo_CT_AbTPRE_OP_AbT, explanationsAHT );
						
						// Create a list of inferences (merge multiple equivalent inferences)
						Set<String> inferencesAHT = getInferences( resultsAHT );
			
						// For each inference from fifth KB
						for( String inferenceAHT : inferencesAHT )
						{
							// Add the current ordered treatments (therapy) to the set of therapies
							List<String> therapy = new ArrayList<String>();
							therapy.add( inferenceCT );
							therapy.add( inferenceAbTPRE );
							therapy.add( inferenceOP );
							therapy.add( inferenceAbT );
							therapy.add( inferenceAHT );
							therapies.add( therapy );
							
							// Add the current ordered explanations
							List<Set<Rule>> therapyExplanation = new ArrayList<Set<Rule>>();
							therapyExplanation.add( explanationsCT );
							therapyExplanation.add( explanationsAbTPRE );
							therapyExplanation.add( explanationsOP );
							therapyExplanation.add( explanationsAbT );
							therapyExplanation.add( explanationsAHT );
							explanations.put( therapy, therapyExplanation );
						}
					}
				}
			}
		}
	
		return therapies;
	}
}
